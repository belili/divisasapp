﻿using GalaSoft.MvvmLight.Command;
using System.ComponentModel;
using System.Windows.Input;

namespace DivisasApp.ViewModels
{
    public class HomeViewModel : INotifyPropertyChanged
    {
        #region Attributes

        private decimal dollars;

        private decimal euros;

        private decimal pounds;

        #endregion

        #region Properties

        public decimal Soles { get; set; }

        public decimal Dollars
        {
            set
            {
                if (dollars != value)
                {
                    dollars = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Dollars"));
                }
            }
            get
            {
                return dollars;
            }
        }

        public decimal Euros
        {
            set
            {
                if (euros != value)
                {
                    euros = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Euros"));
                }
            }
            get
            {
                return euros;
            }
        }

        public decimal Pounds
        {
            set
            {
                if (pounds != value)
                {
                    pounds = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Pounds"));
                }
            }
            get
            {
                return pounds;
            }
        }

        #endregion

        #region Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Commands

        public ICommand ConvertCommand { get { return new RelayCommand(ConvertMoney); } }

        private async void ConvertMoney()
        {
            if (Soles <= 0)
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe ingresar un valor mayor a 0", "Aceptar");

                return;
            }

            Dollars = Soles * (decimal)3.34;

            Euros = Soles * (decimal)3.70;

            Pounds = Soles * (decimal)4.28;
        }

        #endregion
    }
}
