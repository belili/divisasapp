﻿using DivisasApp.ViewModels;

namespace DivisasApp.Infrastructure
{
    public class InstanceLocator
    {
        public HomeViewModel Main { get; set; }

        public InstanceLocator()
        {
            Main = new HomeViewModel();
        }
    }
}
